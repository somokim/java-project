package com.bluemouse.java_training.generics;

public class TestGenerics <A>{
    A any;

    public TestGenerics(A any){
        this.any = any;
    }

    public A getAny(){
        return any;
    }

    public void testGenericsBlock(){
        A lc = this.any;
    }
}
