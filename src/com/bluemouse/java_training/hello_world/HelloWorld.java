package com.bluemouse.java_training.hello_world;

import com.bluemouse.java_training.Animal.Animal;
import com.bluemouse.java_training.generics.TestGenerics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HelloWorld {

    public static void main(String []args){
        //Animal obj = new Animal();
        System.out.println("Hello World"); // prints Hello World
        Date cDate = new Date();
        System.currentTimeMillis();
        //cDate.setTime();
        // 11-June-2019
        genericsMethod(new HelloWorld());
        int obj = genericsMethod(1);
        TestGenerics<HelloWorld> test = new TestGenerics<>(new HelloWorld());

    }

    public static <H> H genericsMethod(H val){
        if(val instanceof HelloWorld){
            ///// ////
        }
        return val;
    }
}
