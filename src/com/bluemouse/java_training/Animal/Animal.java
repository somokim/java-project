package com.bluemouse.java_training.Animal;


public final class Animal {

     public static String STATIC_VAR_NAME = "";

    // Object's fields
    String name;
    private String type;
    private static final String TEST_FINAL = "";
    //

    static {
        System.out.println("Static block has performed!");
        STATIC_VAR_NAME = "";
        //this.name = "";
    }

    // Instant block
    {
        System.out.println("Instant block has performed!");
        this.speak();
        String blockVar = "klsalfksdlfk";

    }

    public Animal(){
        System.out.println("Default Constructor has performed!");
    }

    public Animal(String name, String type){
        this();
        this.name = name;
        this.type = type;
        System.out.println("Parameter Constructor has performed!");
    }

    // Object's method
    public void speak(){
        STATIC_VAR_NAME = "";
        String methodVar = "";
        System.out.println("Hello I am " + this.name);
    }

    public static void testStaticMethod(){
        System.out.println("Hellow form Static Method");
    }

}
